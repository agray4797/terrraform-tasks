// Terraform configuration
terraform {
    required_providers {
        azurerm = {
            source  = "hashicorp/azurerm"
            version = "~> 2.46.0"
        }
    }
}

// Provider
provider "azurerm" {
    features {}
}

// Resource Group
resource "azurerm_resource_group" "main" {
  name     = "vnet-rg"
  location = var.location
}

// Virtual Network module
module "virtual_network" {
  source = "./vnet"
  // variables passed down to child modules here
  group_name   = azurerm_resource_group.main.name
  location     = var.location
  project_name = var.project_name
  
}

// Virtual Machines module
module "virtual_machine" {
  source = "./vm"
  // variables passed down to child modules here
  group_name   = azurerm_resource_group.main.name
  location     = var.location
  project_name = var.project_name
  vm_size      = var.vm_size
}
