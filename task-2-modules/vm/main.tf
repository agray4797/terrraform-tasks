// Public VM
resource "azurerm_public_ip" "firstIP" {
  name                = "DemoIP"
  resource_group_name = var.group_name
  location            = var.location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "public" {
  name                = "public-nic"
  location            = var.location
  resource_group_name = var.group_name
  ip_configuration {
    name                          = "public_ip_config"
    subnet_id                     = azurerm_subnet.public.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.firstIP.id
  }
}

resource "azurerm_linux_virtual_machine" "public" {
  name                = "public_machine"
  resource_group_name = var.group_name
  location            = var.location
  size                = "Standard_B1s"
  admin_username      = "argra"
  network_interface_ids = [
    azurerm_network_interface.public.id,
  ]

  admin_ssh_key {
    username   = "argra"
    public_key = file("C:\\Users\\argra\\.ssh\\id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}

// Private VM
resource "azurerm_public_ip" "private" {
  name                = "DemoIP"
  resource_group_name = var.group_name
  location            = var.location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "private" {
  name                = "private-nic"
  location            = var.location
  resource_group_name = var.group_name

  ip_configuration {
    name                          = "priv_ip_config"
    subnet_id                     = azurerm_subnet.private.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.private.id
  }
}

resource "azurerm_linux_virtual_machine" "private" {
  name                = "private-machine"
  resource_group_name = var.group_name
  location            = var.location
  size                = "Standard_B1s"
  admin_username      = "argra"
  network_interface_ids = [
    azurerm_network_interface.private.id,
  ]

  admin_ssh_key {
    username   = "argra"
    public_key = file("C:\\Users\\argra\\.ssh\\id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}
