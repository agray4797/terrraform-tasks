// Define empty variable blocks – values inherited from module blocks
variable "project_name" {}
variable "location" {}
variable "vm_count" {}
variable "vm_size" {}
variable "storage_size" {}
variable "adminuser" {}
