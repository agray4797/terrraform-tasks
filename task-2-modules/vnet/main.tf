// Virtual Network
resource "azurerm_virtual_network" "main" {
  name                = "${var.project_name}-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = var.location
  resource_group_name = var.group_name
}

// Public Subnet with NSG allowing SSH from everywhere
resource "azurerm_subnet" "public" {
  name                 = "public_net"
  resource_group_name  = var.group_name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_security_group" "public" {
  name                = "public_subnet"
  location            = var.location
  resource_group_name = var.group_name

  security_rule {
    name                       = "publicSSH"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "public" {
  subnet_id                 = azurerm_subnet.public.id
  network_security_group_id = azurerm_network_security_group.public.id
}

// Private Subnet with NSG allowing SSH only from public subnet
resource "azurerm_subnet" "private" {
  name                 = "private_net"
  resource_group_name  = var.group_name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_network_security_group" "private" {
  name                = "private_subnet"
  location            = var.location
  resource_group_name = var.group_name

  security_rule {
    name                       = "privateSSH"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "${azurerm_subnet.private.address_prefixes.0}"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "privateAll"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Deny"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "private" {
  subnet_id                 = azurerm_subnet.private.id
  network_security_group_id = azurerm_network_security_group.private.id
}
